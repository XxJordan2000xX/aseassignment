﻿using System;
using System.Drawing;
using ASEAssignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ASEAssignmentTests
{
    /// <summary>
    ///This class will contain the tests that will be run to
    /// ensure that the code functions as intend and has minimum errors for 
    /// part2 components.
    /// 
    /// This will be helpful as it will allow the programmer to check if an 
    /// alteration they have made has broken some code somewhere else and 
    /// will make it easier to find errors.
    /// </summary>
    [TestClass]
    public class Part2Tests
    {
        //will create an instance of the canvas for which the execution of parmands will be checked against.
        static Bitmap OutputBitmap = new Bitmap(500, 500);
        Canvas c = new Canvas(Graphics.FromImage(OutputBitmap));

        /// <summary>
        /// this method will test that if a valid variable assignment is done then no error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandVariableDeclerationValid()
        {
            Parser par = new Parser();
            bool expected = false;
            String[] tests = { "var cool = 100", "var something" };
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
        /// <summary>
        /// this method will check that if an invalid variable assignment is done then an error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandVariableDeclerationInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            String[] tests = { "var circle = 100", "hello = 11", "var h = hello + text", "var hello = circle" };
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
        /// <summary>
        /// this method will test that if a valid variable update is done then no error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandVariableUpdateValid()
        {
            Parser par = new Parser();
            bool expected = false;
            String test = "var cool = 100" + "\n" + "cool = 150";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found");
        }
        /// <summary>
        /// this method will test that if a invalid variable update is done then an error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandVariableUpdateInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            String test = "var cool = 100" + "\n" + "cools = 150";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found");
        }
        /// <summary>
        /// this method will test that if a valid if statement is passed in then no error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandIfValid()
        {
            //if without variables as params
            Parser par = new Parser();
            bool expected = false;
            String test = "if 200 > 100" + "\n" + "circle 50" + "\n" + "endif";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found ifs no variables");
            //if without variables as params
            String test1 = "var var1 = 100" + "\n" + "if 200 > var1" + "\n" + "circle 50" + "\n" + "endif";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found ifs with variables");
        }
        /// <summary>
        /// this method will test that if a invalid if statement is passed in then an error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandIfInvalid()
        {
            //checks invalid endif caught 
            Parser par = new Parser();
            bool expected = true;
            String test = "if 100 < 200" + "\n" + "circle 150" + "\n" + "endifs";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found in endif");
            //checks invalid if start caught 
            String test1 = "if 100 > mer";
            par.Read(test1, c, false);
            Assert.AreEqual(expected, par.Error, "error found in ifstart");
        }
        /// <summary>
        /// this method will test that if a valid while loop is passed in then no error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandWhileValid()
        {
            //if without variables as params
            Parser par = new Parser();
            bool expected = false;
            String test = "while 200 > 100" + "\n" + "circle 50" + "\n" + "endwhile";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found while no variables");
            //if without variables as params
            String test1 = "var var1 = 100" + "\n" + "while 200 > var1" + "\n" + "circle 50" + "\n" + "var1 = var1 + 10" + "\n" + "endwhile";
            par.Read(test1, c, false);
            Assert.AreEqual(expected, par.Error, "error found while with variables");
        }
        /// <summary>
        /// this method will test that if a invalid while loop is passed in then an error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandWhileInvalid()
        {
            //checks invalid endif caught 
            Parser par = new Parser();
            bool expected = true;
            String test = "while 100 < 200" + "\n" + "circle 150" + "\n" + "endwhiles";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found in endwhile");
            //checks invalid if start caught 
            String test1 = "while 100 > mer";
            par.Read(test1, c, false);
            Assert.AreEqual(expected, par.Error, "error found in while start");
        }
        /// <summary>
        /// this method will test that if a valid method is created then no error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandMethodValid()
        {
            //method with one command inside and no params
            Parser par = new Parser();
            bool expected = false;
            String test = "method lol" + "\n" + "circle 50" + "\n" + "endmethod";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method decleration");
            //method with more than one command inside and no params
            String test1 = "method lol" + "\n" + "circle 50" + "\n" + "rectangle 100,100" + "\n" + "endmethod";
            par.Read(test1, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method decleration");
        }
        /// <summary>
        /// this method will test that if a invalid method is created then an error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandMethodInvalid()
        {
            //checks invalid endmethod 
            Parser par = new Parser();
            bool expected = true;
            String test = "method lol" + "\n" + "circle 50" + "\n" + "endmethods";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method decleration");
            //checks invalid method name  
            String test1 = "method 100";
            par.Read(test1, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method decleration");
        }
        /// <summary>
        /// this method will test that if a valid method is called then no error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandMethodCallValid()
        {
            //method with one command inside and no params
            Parser par = new Parser();
            bool expected = false;
            String test = "method lol" + "\n" + "circle 50" + "\n" + "endmethod" + "\n" + "call lol";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method call");
            //method with more than one command inside and no params
            String test1 = "method a" + "\n" + "circle 50" + "\n" + "rectangle 100,150" + "\n" + "endmethod" + "\n" + "call a";
            par.Read(test1, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method call");
        }
        /// <summary>
        /// this method will test that if a invalid method is called then an error will be found.
        /// </summary>
        [TestMethod]
        public void TestcommandMethodCallInvalid()
        {
            //checks invalid endmethod 
            Parser par = new Parser();
            bool expected = true;
            String test = "method lol" + "\n" + "circle 50" + "\n" + "endmethod" + "\n" + "call boo";
            par.Read(test, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method call");
            //checks invalid method name  
            String test1 = "call fish";
            par.Read(test1, c, false);
            Assert.AreEqual(expected, par.Error, "error found in method call");
        }
        /// <summary>
        /// this method will check that a valid square command will not catch any errors.
        /// </summary>
        [TestMethod]
        public void TestCommandSquareParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("square 100", c, false);

            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }
        /// <summary>
        /// this method will test that an error is caught when an invalid square commmand is parsed.
        /// </summary>
        [TestMethod]
        public void TestCommandSquareParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = { "square", "square -100", "square 100,100", "square 0", "square 10.2" };
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
        /// <summary>
        /// this method will check that a valid arc command will not catch any errors.
        /// </summary>
        [TestMethod]
        public void TestCommandArcParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("arc 100,100,60,50", c, false);

            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }
        /// <summary>
        /// this method will test that an error is caught when an invalid arc commmand is parsed.
        /// </summary>
        [TestMethod]
        public void TestCommandArcParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = { "arc", "arc -100,100,50,60",
                               "arc 100,100", "arc 0,100,100,100",
                               "arc 10.2,100,20,40",
                               "arc 100,100,361,20",
                               "arc 100,100,20,361",
                               "arc 100,100,60"};
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
    }
}
