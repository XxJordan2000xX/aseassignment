﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEAssignment;
using System.Drawing;

namespace ASEAssignmentTests
{
    /// <summary>
    /// This class will contain the tests that will be run to
    /// ensure that the code functions as intend and has minimum errors for 
    /// part1 components.
    /// 
    /// This will be helpful as it will allow the programmer to check if an 
    /// alteration they have made has broken some code somewhere else and 
    /// will make it easier to find errors.
    /// </summary>

    [TestClass]
    public class Part1Tests
    {
        //the bitmap is created here as it wil be used to test the majority of the programs functionality.
        static Bitmap OutputBitmap = new Bitmap(500, 500);
        Canvas c = new Canvas(Graphics.FromImage(OutputBitmap));
        
        /// <summary>
        /// This method will test the moveto class and its ability to execute the correct input,
        ///by setting the x and y cordinates to the appropriate values.
        /// </summary>
        ///
        [TestMethod]
     
        public void TestCommandMoveToValidExecution()
        {
            MoveTo mt = new MoveTo();
            int expected = 100;
            int X = 100;
            int Y = 100;

            //action calling commandsclass to test correct input and in turn makes
            //a call to the moveTo class to execute command.
            mt.positionPen(X, Y, c);


            //asserts equal to test values 
            Assert.AreEqual(expected, c.X, 0.1, "moveto not executed correctly X coordinate incorrect");
            Assert.AreEqual(expected, c.Y, 0.1, "moveto not executed correctly Y coordinate incorrect");

        }
        /// <summary>
        /// This method will test the drawto class and its ability to execute the correct input,
        /// by checking that the x and y coordinates have been set appropriately.
        /// </summary>
        [TestMethod]
        public void TestCommandDrawToValidExecution()
        {
            DrawTo dt = new DrawTo();
            int expected = 50;
            int X = 50;
            int Y = 50;

            //action calling commandsclass to test correct input and in turn makes
            //a call to the DrawTo class to execute command.
            dt.Drawline(X, Y, c);

            //asserts equal to test values 
            Assert.AreEqual(expected, c.X, 0.1, "drawto not executed correctly x coordinate wrong");
            Assert.AreEqual(expected, c.Y, 0.1, "drawto not executed correctly y coordinate wrong");
        }
        /// <summary>
        /// This method will test the Reset class and its ability execution appropriatley for the correct
        /// reset command by ensuring the values for x and y are both set to 0.
        /// </summary>
        [TestMethod]
        public void TestCommandResetValidExecution()
        {
            Reset r = new Reset();
            DrawTo d = new DrawTo();
            int expected = 0;
            int x = 100, y = 100;

            //calls execution of method
            d.Drawline(x, y,c);
            r.ResetGraphic(c);

            //asserts equal to test values 
            Assert.AreEqual(expected, c.X, 0.1, "reset not executed correctly x coordinate wrong");
            Assert.AreEqual(expected, c.Y, 0.1, "reset not executed correctly y coordinate wrong");
        }

        /// <summary>
        /// This will check that the move to command doesn't catch an error when the correct input is passed in.
        /// </summary>
        [TestMethod]
        public void TestCommandMoveToParseValid()
        {
            Parser par = new Parser();
            bool expected_error = false;

            //execute valid command
            par.Read("moveto 100,100",c,false);

            //check that command is valid
            Assert.AreEqual(expected_error, par.Error,"error found");

        }

        /// <summary>
        /// This method will catch errors for the majority of cases in which the moveto command is incorrectly called.
        /// </summary>
        [TestMethod]
        public void TestCommandMoveToParseInValid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {"movetoo", "moveto 100", "moveto -100,10","moveto 10.5,120"};
            foreach(var test in tests)
            {
                par.Read(test,c, false);
                Assert.AreEqual(expected, par.Error,"error found");
            }
        }
        /// <summary>
        /// This method will test that the drawto command when correctly input is parsed correctly.
        /// </summary>
        [TestMethod]
        public void TestCommandDrawToParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("drawto 100,125",c, false);
            
            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }

        /// <summary>
        /// this method will test that when a draw command is incorrectly input the error will be caught.
        /// </summary>
        [TestMethod]
        public void TestCommandDrawToParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {"drawtoo", "drawto 100", "drawto -100,10", "drawto 100,100.1"};
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
        /// <summary>
        /// this method will test that when correctly passed the clear command shouldn't find an error.
        /// </summary>
        [TestMethod]
        public void TestCommandClearParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("clear", c, false);

            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }
        /// <summary>
        /// this method will test that when an incorrect clear command is parsed that an error will be caught.
        /// </summary>
        [TestMethod]
        public void TestCommandClearParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {"clears", "clear 100", "clear now"};
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }

        /// <summary>
        /// this method will test that no error is found when the reset key word is parsed. 
        /// </summary>
        [TestMethod]
        public void TestCommandResetParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("reset", c, false);

            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }

        /// <summary>
        /// this method will test that when an incorrect reset command is parsed that the error will be caught.
        /// </summary>
        [TestMethod]
        public void TestCommandResetParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {"resets", "reset 100", "reset now" };
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }

        /// <summary>
        /// this command will test the cases for a valid rectangle command being 
        /// passed and ensuring that it does so correctly without catching an error.
        /// </summary>
        [TestMethod]
        public void TestCommandRectangleParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("rectangle 100,100", c, false);

            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }

        /// <summary>
        /// this method will test a majority of possible
        /// cases for an invalid rectangle command to ensure that it catches the errors.
        /// </summary>
        [TestMethod]
        public void TestCommandRectangleParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {"rectangle", "rectangle 100", "rectangle -100,100","rectangle 100,0","rectangle 10.5,10" };
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }

        /// <summary>
        /// this method will check tht a valid circle command will not catch any errors.
        /// </summary>
        [TestMethod]
        public void TestCommandCircleParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("circle 100", c, false);

            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }

        /// <summary>
        /// this method will test that an error is caught when an invalid circle commmand is parsed.
        /// </summary>
        [TestMethod]
        public void TestCommandCircleParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {"circle", "circle -100", "circles 100,100", "circle 0","circle 10.2"};
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }

        /// <summary>
        /// this method will ensure that no errors are caught when a valid triangle command is parsed.
        /// </summary>
        [TestMethod]
        public void TestCommandTriangleParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            par.Read("triangle 100,100,60", c, false);

            //check that command is valid
            Assert.AreEqual(expected, par.Error, "error found");
        }

        /// <summary>
        ///this method will test that when an invalid triangle command is parsed that the error will be caught. 
        /// </summary>
        [TestMethod]
        public void TestCommandTriangleParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = { 
                "triangle", "triangle -100,100,90",
                "triangle 100,100,190","trinangle 100,100,0",
                "triangle 100,50","triangle 100,100,1.5","100,100,-5"};
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
        /// <summary>
        /// this mehtod will test that all the valid pen colours are parsed without catching an error.
        /// </summary>
        [TestMethod]
        public void TestCommandPenParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            string[] tests = {
                "pen green", "pen red", "pen black","pen blue" };

            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }

        /// <summary>
        /// this method will ensure that an invalid pen method is caught and error returns true.
        /// </summary>
        [TestMethod]
        public void TestCommandPenParseInvalid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {
                "pen", "pens",
                "pen 100","pen blue too"};
            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }

        /// <summary>
        /// this method will test that a valid fill command doesn't catch an error.
        /// </summary>
        [TestMethod]
        public void TestCommandFillParseValid()
        {
            Parser par = new Parser();
            bool expected = false;
            string[] tests = {
                "fill on", "fill off"};

            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
        /// <summary>
        /// this method tests to see that if an invalid fill command is parsed than an error will be caught.
        /// </summary>
        [TestMethod]
        public void TestCommandFillParseInValid()
        {
            Parser par = new Parser();
            bool expected = true;
            string[] tests = {
                "fill ons", "fill offf","fills","fill 20","fill","fill on 20"};

            foreach (var test in tests)
            {
                par.Read(test, c, false);
                Assert.AreEqual(expected, par.Error, "error found");
            }
        }
    }
}
