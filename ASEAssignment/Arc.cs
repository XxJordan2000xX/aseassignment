﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// this class holds the code responsible for drawing arcs.
    /// </summary>
    class Arc
    {
        /// <summary>
        /// This method will draw an arc 
        /// and in doing so update the values of x and y coordinates in the canvas class,
        /// so that the next thing will be drawn from the end point of this line.
        /// </summary>
        /// <param name="width">the width of the arc</param>
        /// <param name="height">the height of the are</param>
        /// <param name="startAngle">the angle in degrees clockwise from the x-axis to the starting point of the arc</param>
        /// <param name="SweepAngle">the angle in degrees measured clockwise from the startAngle parameter to ending point of the arc</param>
        /// <param name="MyCanvas">the graphical context which will be updated.</param>
        public void DrawArc(int width, int height, int startAngle, int SweepAngle,Canvas MyCanvas)
        {
            MyCanvas.g.DrawArc(MyCanvas.pen, MyCanvas.X, MyCanvas.Y, width, height,startAngle,SweepAngle);
            //new x and y
            MyCanvas.X =  width;
            MyCanvas.Y = height;
        }
    }
}
