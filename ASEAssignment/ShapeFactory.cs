﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// The factory class is responsible for creating the shapes when called upon.
    /// </summary>
    class ShapeFactory
    {
        /// <summary>
        /// The GetShape method will get the shape which is being called.
        /// </summary>
        /// <param name="shapeType">the shape which is being requested to draw</param>
        /// <returns>makes a call to the class responsible for drawing that shape</returns>
        public ShapeInterface GetShape(String shapeType)
        {
            shapeType.ToLower().Trim();

            if(shapeType.Equals("circle"))
            {
                return new Circle();
            }
            else if(shapeType.Equals("rectangle"))
            {
                return new Rectangle();
            }
            else if (shapeType.Equals("triangle"))
            {
                return new Triangle();
            }
            else if (shapeType.Equals("square"))
            {
                return new Square();
            }
            else
            {
                //throw exception shape type doesn't exist
                System.ArgumentException argEx = new System.ArgumentException("factory error" + shapeType + "shape doesn't exist");
                throw argEx;
            }
        }
    }
}
