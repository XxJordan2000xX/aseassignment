﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ASEAssignment
{
    /// <summary>
    /// this class is responcible for calling the execution of the commands
    /// </summary>
    class StoredCode : ArrayList
    {
        //creates an instance of the shape factory that can be called for drawing shapes
        ShapeFactory factory = new ShapeFactory();
        Shape s;

        private int currentline = 0; //where to go back to after method has execute
        private bool MethodFlag = false; // is a method being declaired 
        private bool MethodExecuting = false; // is the method executing
        private ArrayList MethodNames = new ArrayList(); // array of the method names
        private ArrayList Methodpositions = new ArrayList(); // array of the method start positions

        private bool IfFlag = false; // are we in an if statement
        
        private bool WhileFlag = false; // are we in a loop
        private int loopStart = 0; //holds the position of the start of the loop
        private bool loopCondition = false; //is the loop condition true
            
        int i = 0;//value used as index for the array lists to access values at specific indexes
        private Dictionary<string, int> Variables = new Dictionary<string, int>();// stores the variable name and corrosponding value  
       
        /// <summary>
        /// will call the execution of the valid commands that are passed to this method 
        /// </summary>
        /// <param name="Commands">an arraylist of the commands</param>
        /// <param name="Paramaters">an arraylist of the associated paramaters</param>
        /// <param name="MyCanvas">the graphics context which is being updated</param>
        public void process(ArrayList Commands, ArrayList Paramaters, Canvas MyCanvas)
        {
            //while i is < number of command
            while (i < Commands.Count)
            {
                if (Commands[i].ToString().Equals("endmethod"))
                {
                    //encountered end method when calling the method 
                    if (MethodExecuting)
                    {
                        MethodExecuting = false;
                        //sets program counter to the next line to execute
                        i = currentline;
                        continue;

                    }
                    //encountered the endmethod keyword when defining the method
                    else
                    {
                        MethodFlag = false;
                    }
                }
                else if (Commands[i].ToString().Equals("endwhile"))
                {
                    //not executing while
                    if (WhileFlag)
                    {
                        WhileFlag = false;
                        i++;
                        continue;
                    }
                    //jump to start of loop
                    else
                    {
                        i = loopStart;
                        loopCondition = false;
                        continue;
                    }
                }
                else if (Commands[i].ToString().Equals("endif"))
                {
                    //end of the if statement
                    IfFlag = false;
                    i++;
                    continue;
                }
                if (MethodFlag)
                {
                    i++;
                    //inside method decleration no execution of code
                    continue;
                }
                if (IfFlag)
                {
                    //will be called if the if statement condition is false
                    i++;
                    continue;
                }
                if (WhileFlag)
                {
                    i++;
                    continue;
                }
                else if (Commands[i].ToString().Equals("var"))
                {
                    //decleration of variable
                    string[] var = Paramaters[i].ToString().Split('=', ' ');
                    string varName = var[0]; //variable name
                    int number = 0;
                    int last = 0; //last value
                    int total = 0;//total of variable value
                    int count = 0;
                    string op = " "; //operand
                    //removes variable name
                    int remove = 0;
                    var = var.Where((source, index) => index != remove).ToArray();
                    foreach (var value in var)
                    {
                        bool success = int.TryParse(value, out number);
                        if (success)
                        {
                            if (count == 0)
                            {
                                total = number;
                            }
                            else
                            {
                                last = number;
                            }
                        }
                        else
                        {
                            if (Variables.ContainsKey(value))
                            {
                                if (count == 0)
                                {
                                    total = Variables[value];
                                }
                                else
                                {
                                    //get value for that variable and add it to numeric expression
                                    last = Variables[value];
                                }
                            }
                            else if (value == "+")
                            {
                                op = "+";
                            }
                            else if (value == "-")
                            {
                                op = "-";
                            }
                            else if (value == "*")
                            {
                                op = "*";
                            }
                            else if (value == "/")
                            {
                                op = "/";
                            }
                            else if (value == "%")
                            {
                                op = "%";
                            }
                        }
                        if (count % 2 == 0)
                        {
                            if (op == "+")
                            {
                                total = total + last;
                            }
                            else if (op == "-")
                            {
                                total = total - last;
                            }
                            else if (op == "*")
                            {
                                total = total * last;
                            }
                            else if (op == "/")
                            {
                                total = total / last;
                            }
                            else if (op == "%")
                            {
                                total = total % last;
                            }
                        }
                        count++;
                    }
                    Variables.Add(varName, total);
                }
                else if (Variables.ContainsKey(Commands[i].ToString()))
                {
                    //get current value and update with new value
                    string[] expression = Paramaters[i].ToString().Split(' ');
                    int number = 0;
                    int total = 0;
                    int last = 0;
                    int count = 0;
                    string op = " "; //operand
                    foreach (var exp in expression)
                    {
                        bool success = int.TryParse(exp, out number);
                        if (success)
                        {
                            if (count == 0)
                            {
                                total = number;
                            }
                            else
                            {
                                last = number;
                            }                          
                        }
                        else
                        {
                            if (Variables.ContainsKey(exp))
                            {
                                if (count == 0)
                                {
                                    total = Variables[exp];
                                }
                                else
                                {
                                    //get value for that variable and add it to numeric expression
                                    last = Variables[exp];
                                }
                            }
                            else if (exp == "+")
                            {
                                op = "+";
                            }
                            else if (exp == "-")
                            {
                                op = "-";
                            }
                            else if (exp == "*")
                            {
                                op = "*";
                            }
                            else if (exp == "/")
                            {
                                op = "/";
                            }
                            else if (exp == "%")
                            {
                                op = "%";
                            }
                        }
                        if (count % 2 == 0)
                        {
                            if (op == "+")
                            {
                                total = total + last;
                            }
                            else if (op == "-")
                            {
                                total = total - last;
                            }
                            else if (op == "*")
                            {
                                total = total * last;
                            }
                            else if (op == "/")
                            {
                                total = total / last;
                            }
                            else if (op == "%")
                            {
                                total = total % last;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        count++;
                    }
                    Variables[Commands[i].ToString()] = total;
                }
                else if (Commands[i].ToString().Equals("method"))
                {
                    if (MethodExecuting)
                    {
                        i++;
                        continue;
                    }
                    else
                    {
                        //method decleration (must be declaired before it can be used)
                        string[] declareMethod = Paramaters[i].ToString().Split(',');
                        //adds the method name and counter at that point to a dictionary 
                        MethodNames.Add(declareMethod[0]);
                        Methodpositions.Add(i);
                        //method is being declaired
                        MethodFlag = true;
                    }

                }
                else if (Commands[i].ToString().Equals("while"))
                {
                    //split the paramaters on a space 
                    string[] conditionParams = Paramaters[i].ToString().Split(' ');
                    ArrayList values = new ArrayList();
                    string Numbers = "";
                    int Count = 0;
                    int number = 0;
                    string comparitor = "";
                    foreach (var param in conditionParams)
                    {
                        //even
                        if(Count%2 == 0)
                        {
                            bool success = int.TryParse(param, out number);
                            if (success)
                            {
                                //its an integer
                                values.Add(number);
                            }
                            else
                            {
                                //its a variable 
                                values.Add(Variables[param]);
                            }
                        }
                        //odd
                        else
                        {
                            comparitor = comparitor + param;                         
                        }
                        Count++;                      
                    }
                    foreach (var val in values)
                    {
                        Numbers = Numbers + val + " ";
                    }
                    string[] comparingNumbers = Numbers.Split(' ');
                    //check if condition is true 
                    if (comparitor == "<")
                    {
                        if(Int32.Parse(comparingNumbers[0]) < Int32.Parse(comparingNumbers[1]))
                        {
                            loopCondition = true;
                        }
                        else
                        {
                            loopCondition = false;
                        }
                    }
                    else if (comparitor == ">")
                    {
                        if (Int32.Parse(comparingNumbers[0]) > Int32.Parse(comparingNumbers[1]))
                        {
                            loopCondition = true;
                        }
                        else
                        {
                            loopCondition = false;
                        }
                    }
                    else if (comparitor == "<=")
                    {
                        if (Int32.Parse(comparingNumbers[0]) <= Int32.Parse(comparingNumbers[1]))
                        {
                            loopCondition = true;
                        }
                        else
                        {
                            loopCondition = false;
                        }
                    }
                    else if (comparitor == ">=")
                    {
                        if (Int32.Parse(comparingNumbers[0]) >= Int32.Parse(comparingNumbers[1]))
                        {
                            loopCondition = true;
                        }
                        else
                        {
                            loopCondition = false;
                        }
                    }
                    else if (comparitor == "==")
                    {
                        if (Int32.Parse(comparingNumbers[0])  == Int32.Parse(comparingNumbers[1]))
                        {
                            loopCondition = true;
                        }
                        else
                        {
                            loopCondition = false;
                        }
                    }
                    else if (comparitor == "!=")
                    {
                        if (Int32.Parse(comparingNumbers[0]) != Int32.Parse(comparingNumbers[1]))
                        {
                            loopCondition = true;
                        }
                        else
                        {
                            loopCondition = false;
                        }
                    }
                    //is the loop executing or not
                    if (loopCondition)
                    {
                        loopStart = i;
                        i++;
                        continue;
                    }
                    else
                    {
                        WhileFlag = true;
                        continue;
                    }
                }
                else if (Commands[i].ToString().Equals("if"))
                {
                    //split the paramaters on a space 
                    string[] conditionParams = Paramaters[i].ToString().Split(' ');
                    ArrayList values = new ArrayList();
                    string Numbers = "";
                    int Count = 0;
                    int number = 0;
                    string comparitor = "";
                    foreach (var param in conditionParams)
                    {
                        //even
                        if (Count % 2 == 0)
                        {
                            bool success = int.TryParse(param, out number);
                            if (success)
                            {
                                //its an integer
                                values.Add(number);
                            }
                            else
                            {
                                //its a variable 
                                values.Add(Variables[param]);
                            }
                        }
                        //odd
                        else
                        {
                            comparitor = comparitor + param;
                        }
                        Count++;
                    }
                    foreach (var val in values)
                    {
                        Numbers = Numbers + val + " ";
                    }
                    string[] comparingNumbers = Numbers.Split(' ');
                    //check if condition is true 
                    if (comparitor == "<")
                    {
                        if (Int32.Parse(comparingNumbers[0]) < Int32.Parse(comparingNumbers[1]))
                        {
                            IfFlag = false;
                        }
                        else
                        {
                            IfFlag = true;
                        }
                    }
                    else if (comparitor == ">")
                    {
                        if (Int32.Parse(comparingNumbers[0]) > Int32.Parse(comparingNumbers[1]))
                        {
                            IfFlag = false;
                        }
                        else
                        {
                            IfFlag = true;
                        }
                    }
                    else if (comparitor == "<=")
                    {
                        if (Int32.Parse(comparingNumbers[0]) <= Int32.Parse(comparingNumbers[1]))
                        {
                            IfFlag = false;
                        }
                        else
                        {
                            IfFlag = true;
                        }
                    }
                    else if (comparitor == ">=")
                    {
                        if (Int32.Parse(comparingNumbers[0]) >= Int32.Parse(comparingNumbers[1]))
                        {
                            IfFlag = false;
                        }
                        else
                        {
                            IfFlag = true;
                        }
                    }
                    else if (comparitor == "==")
                    {
                        if (Int32.Parse(comparingNumbers[0]) == Int32.Parse(comparingNumbers[1]))
                        {
                            IfFlag = false;
                        }
                        else
                        {
                            IfFlag = true;
                        }
                    }
                    else if (comparitor == "!=")
                    {
                        if (Int32.Parse(comparingNumbers[0]) != Int32.Parse(comparingNumbers[1]))
                        {
                            IfFlag = false;
                        }
                        else
                        {
                            IfFlag = true;
                        }
                    }
                    //is the if executing or not
                    if (!IfFlag)
                    {
                        i++;
                        continue;
                    }
                }             
                else if (Commands[i].ToString().Equals("call"))
                {
                    //sets current line to next line so that when program returns it carries on 
                    currentline = i + 1;
                    //sets program counter the value of the where the method is declaired
                    //splits paramaters
                    string[] call = Paramaters[i].ToString().Split(',');
                    //gets index value of method name
                    i = (int)Methodpositions[MethodNames.IndexOf(call[0])];
                    //now executing the method
                    MethodExecuting = true;
                    continue;
                }
                else if (Commands[i].ToString().Equals("moveto"))
                {
                    string[] penmove = Paramaters[i].ToString().Split(',');
                    ArrayList values = new ArrayList(); //paramaters for draw commands
                    string coordinates = "";
                    //get the length and the width
                    foreach (var pm in penmove)
                    {
                        Variable var = new Variable();
                        int value = var.CurrentValue(pm, Variables);
                        values.Add(value);
                    }
                    foreach (var val in values)
                    {
                        coordinates = coordinates + val + " ";
                    }
                    string[] inputs = coordinates.Split(' ');
                    //moves pen from last point to this point.
                    MoveTo move = new MoveTo();
                    move.positionPen(Int32.Parse(inputs[0]), Int32.Parse(inputs[1]), MyCanvas);
                }
                else if (Commands[i].ToString().Equals("drawto"))
                {
                    String[] pendraw = Paramaters[i].ToString().Split(',');
                    string coordinates = "";
                    ArrayList values = new ArrayList(); //paramaters for draw commands
                    //get the length and the width
                    foreach (var pd in pendraw)
                    {
                        Variable var = new Variable();
                        int value = var.CurrentValue(pd, Variables);
                        values.Add(value);
                    }
                    foreach (var val in values)
                    {
                        coordinates = coordinates + val + " ";
                    }
                    string[] inputs = coordinates.Split(' ');
                    //draw line from last point to this point.
                    DrawTo dr = new DrawTo();
                    dr.Drawline(Int32.Parse(inputs[0]), Int32.Parse(inputs[1]), MyCanvas); 
                }
                else if (Commands[i].ToString().Equals("clear"))
                {
                    //will clear the canvas
                    Clear cle = new Clear();
                    cle.ClearGraphic(MyCanvas);
                }
                else if (Commands[i].ToString().Equals("reset"))
                {
                    //resets pen to initial position
                    Reset res = new Reset();
                    res.ResetGraphic(MyCanvas);
                }
                else if (Commands[i].ToString().Equals("fill"))
                {
                    //toggle fill on or off
                    Colour fillc = new Colour();
                    fillc.fillColour(Paramaters[i].ToString(), MyCanvas);
                }
                else if (Commands[i].ToString().Equals("pen"))
                {
                    // change pen colour to that specified
                    Colour penc = new Colour();
                    penc.penColour(Paramaters[i].ToString(), MyCanvas);
                }
                else if (Commands[i].ToString().Equals("circle"))
                {
                    //get the radius of the circle
                    Variable var = new Variable();
                    int radius = var.CurrentValue(Paramaters[i].ToString(), Variables);
                    //draw circle with these peramaters!
                    s = (Shape)factory.GetShape("circle");
                    s.set(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.X, MyCanvas.Y, radius);
                    s.draw(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.g);
                }
                else if (Commands[i].ToString().Equals("rectangle"))
                {
                    String[] rectangle = Paramaters[i].ToString().Split(',');
                    string wh = "";
                    ArrayList values = new ArrayList(); //paramaters for draw commands
                    //get the length and the width
                    foreach (var rec in rectangle)
                    {
                        Variable var = new Variable();
                        int value = var.CurrentValue(rec, Variables);
                        values.Add(value);
                    }
                    foreach (var val in values)
                    {
                        wh = wh + val + " ";
                    }
                    string[] inputs = wh.Split(' ');
                    //draw rectangle with these peramaters!
                    s = (Shape)factory.GetShape("rectangle");
                    s.set(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.X, MyCanvas.Y, Int32.Parse(inputs[0]), Int32.Parse(inputs[1]));
                    s.draw(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.g);
                }
                else if (Commands[i].ToString().Equals("triangle"))
                {
                    //converts the paraters to a string and splits them
                    String[] triangle = Paramaters[i].ToString().Split(',');
                    string lengthAngle= "";
                    ArrayList values = new ArrayList(); //paramaters for draw commands
                    //get the lengthAB, lengthAC and angleA
                    foreach (var tri in triangle)
                    {
                        Variable var = new Variable();
                        int value = var.CurrentValue(tri, Variables);
                        values.Add(value);
                    }
                    foreach(var val in values)
                    {
                        lengthAngle = lengthAngle + val + " ";
                    }
                    string[] inputs = lengthAngle.Split(' ');
                    //draw rectangle with these peramaters!
                    s = (Shape)factory.GetShape("triangle");
                    s.set(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.X, MyCanvas.Y, Int32.Parse(inputs[0]), Int32.Parse(inputs[1]), Int32.Parse(inputs[2]));
                    s.draw(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.g);
                }
                else if (Commands[i].ToString().Equals("square"))
                {
                    //get the radius of the circle
                    Variable var = new Variable();
                    int widthHeight = var.CurrentValue(Paramaters[i].ToString(), Variables);
                    //draw circle with these peramaters!
                    s = (Shape)factory.GetShape("square");
                    s.set(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.X, MyCanvas.Y, widthHeight);
                    s.draw(MyCanvas.fill, MyCanvas.pen.Color, MyCanvas.g);
                }
                else if (Commands[i].ToString().Equals("arc"))
                {
                    String[] pendraw = Paramaters[i].ToString().Split(',');
                    string coordinates = "";
                    ArrayList values = new ArrayList(); //paramaters for draw commands
                    //get the width, height, startAngle, SweepAngle
                    foreach (var pd in pendraw)
                    {
                        Variable var = new Variable();
                        int value = var.CurrentValue(pd, Variables);
                        values.Add(value);
                    }
                    foreach (var val in values)
                    {
                        coordinates = coordinates + val + " ";
                    }
                    string[] inputs = coordinates.Split(' ');
                    //draw line from last point to this point.
                    Arc A = new Arc();
                    A.DrawArc(Int32.Parse(inputs[0]), Int32.Parse(inputs[1]), Int32.Parse(inputs[2]), Int32.Parse(inputs[3]), MyCanvas);
                }
                    i++;
            }
        }
    }
}
