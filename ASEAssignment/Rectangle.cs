﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// This class is responsible for drawing a rectangle on the form panel.
    /// </summary>
    class Rectangle : Shape
    {
        int width; // stores the width of the rectangle
        int height;//stores the height of the rectangle 
        /// <summary>
        /// blank rectangle constructor
        /// </summary>
        public Rectangle() : base()
        {
            width = 100;
            height = 50;
        }
        /// <summary>
        /// This method contains the constructor for the rectangle class 
        /// </summary>
        /// <param name="MyCanvas">used to get the variables which are being updated from the canvas which is being updated</param>
        /// <param name="width">will hold the width of the shape </param>
        /// <param name="height">will hold the height of the shape</param>
        public Rectangle(Canvas MyCanvas, int width, int height) : base(MyCanvas)
        {
            this.width = width;
            this.height = height;
        }
        /// <summary>
        /// This method will be used to set the shapes peramaters 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="list">variable list as some shapes have more peramaters than others</param>
        public override void set(bool fill, Color colour, params int[] list)
        {
            //list[0] is X, list[1] is Y, list[2] is width, list[3] is height
            base.set(fill, colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }
        /// <summary>
        /// Will determain if it needs to draw a rectangle filled or unfilled. Then it will 
        /// use the draw/fill Rectangle feature in the graphics to draw the shape. 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="g">The graphics context which the shape will be drawn onto</param>
        public override void draw(bool fill, Color colour, Graphics g)
        {
            Pen pen = new Pen(colour, 1);
            SolidBrush brush = new SolidBrush(Color.Black);
            if (fill)
            {
                g.FillRectangle(brush,X,Y,width,height); // will draw a filled rectangle
            }
            else  
            {
                g.DrawRectangle(pen,X,Y,width,height);//will draw a rectangle
            }
            
        }
    }
}
