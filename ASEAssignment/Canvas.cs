﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// this class will be responsible for housing the code related to the
    /// graphics that will be displayed on the form.
    /// </summary>
    public class Canvas
    {
        private Pen Mypen; // the pen that will draw on the canvas
        private Graphics graphic; // the graphics context
        private Brush Mybrush; // the brush that will be used to fill shapes

        private int startX, startY; //will hold the current x and y positions
        private bool fillColor = false;//will save if the shape is to be filled or not 

        /// <summary>
        /// This method contains the getters and the setters for the X coordinate the pen is at
        /// the reason behind doing this is so that when commands are executed this value will be updated 
        /// and the new start position of the pen will be moved to this point.
        /// </summary>
        public int X
        {
            get
            {
                return startX;
            }
            set
            {
                startX = value;
            }
        }
        /// <summary>
        /// This method contains the getters and the setters for the Y coordinate the pen is at
        /// the reason behind doing this is so that when commands are executed this value will be updated 
        /// and the new start position of the pen will be moved to this point.
        /// </summary>
        public int Y
        {
            get
            {
                return startY;
            }
            set
            {
                startY = value;
            }
        }
        /// <summary>
        /// This method contains the getter and setter for the pen this is done so that the pen can be 
        /// used to draw from other classes it also allows for the colour of the pen to be changed.
        /// </summary>
        public Pen pen
        {
            get
            {
                return Mypen;
            }
            set
            {
                Mypen = value;
            }
        }
        /// <summary>
        /// This method contains the getters and setters for the graphic,
        /// this allows other classes to execute commands that will update the graphic which is displayed
        /// on the UI panel.
        /// </summary>
        public Graphics g
        {
            get
            {
                return graphic;
            }
            set
            {
                graphic = value;
            }
        }
        /// <summary>
        /// This method contains the getters and setters for the fill boolean operator this is so the 
        /// state of whether to use a pen or a brush to either draw or fill a shape can be turned on or off.
        /// </summary>
        public bool fill
        {
            get
            {
                return fillColor;
            }
            set
            {
                fillColor = value;
            }
        }
        /// <summary>
        /// This method contains the getters and setters for the brush, this is required for filling shapes.
        /// </summary>
        public Brush brush
        {
            get
            {
                return Mybrush;
            }
            set
            {
                Mybrush = value;
            }
        }


        /// <summary>
        /// This method is a Constructor that initialises the canvas,
        /// with the pen at position(0,0)
        /// </summary>
        /// <param name="g">Grahical element of the form</param>
        public Canvas(Graphics g)
        {
            this.graphic = g;
            startX = startY = 0 ;
            Mypen = new Pen(Color.Black, 1); //initalises pen
            Mybrush = new SolidBrush(Color.Black); // initalises brush
        }
    }
}
