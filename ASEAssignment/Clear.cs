﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// this class is used for clearing the drawing area of the form.
    /// </summary>
    public class Clear
    {
        /// <summary>
        /// This method will take the current canvas and then clear the canvas and set it to white.
        /// </summary>
        /// <param name="MyCanvas">the canvas context that will be updated.</param>
        public void ClearGraphic(Canvas MyCanvas)
        {
            MyCanvas.g.Clear(Color.White); //clears the drawing area
        }
    }
}
