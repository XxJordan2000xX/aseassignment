﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// This class holds the code that will be called for the DrawTo command.
    /// This will cause the canvas to draw a line from The current point to the next point.
    /// </summary>
    public class DrawTo
    {
        /// <summary>
        /// This method will draw a line from the old position(x,y) to new position(x,y)
        /// and in doing so update the values of these coordinates in the canvas class,
        /// so that the next thing will be drawn from the end point of this line.
        /// </summary>
        /// <param name="newX">the X coordinate to move to</param>
        /// <param name="newY">the Y coordinate to move to</param>
        /// <param name="MyCanvas">the graphical context which will be updated.</param>
        public void Drawline(int newX,int newY, Canvas MyCanvas)
        {
            MyCanvas.g.DrawLine(MyCanvas.pen, MyCanvas.X ,MyCanvas.Y,newX,newY);
            MyCanvas.X = newX;
            MyCanvas.Y = newY;
        }
    }
}
