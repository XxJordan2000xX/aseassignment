﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// This abstract class holds the minimul requirements needed to add a derived shape class to the program.
    /// </summary>
    abstract class Shape: ShapeInterface
    {
        //variables are declared as protected so that they are passed through the hierarchy.
        protected int X, Y; //x and y positions to draw the shape from.
        protected bool fill; // if fill is on or off.
        protected Color colour;// the shapes colour

        /// <summary>
        /// blank constructor 
        /// </summary>
        public Shape()
        {
            colour = Color.Black;
            X = Y = 100;
        }
        /// <summary>
        /// Shape constructor, declaring the variables that it can at this stage.
        /// </summary>
        /// <param name="MyCanvas">The graphic context from which the information is taken. </param>
        public Shape(Canvas MyCanvas)
        {
            X = MyCanvas.X;
            Y = MyCanvas.Y; 
            colour = MyCanvas.pen.Color; 
            fill = MyCanvas.fill;
        }

        /// <summary>
        /// This method will do the general set up for the shape set method. 
        /// However, it needs to be overridden by the child version so that it is more specific 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="list">variable list as some shapes have more peramaters than others</param>
        public virtual void set(bool fill, Color colour, params int[] list)
        {
            this.fill = fill;
            this.colour = colour;
            X = list[0];
            Y = list[1];
        }

        /// <summary>
        /// Any derived class must implement a draw method for drawing the shape onto the canvas.
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="g">The graphics context which the shape will be drawn onto</param>
        public abstract void draw(bool fill,Color colour, Graphics g);
    }
}
