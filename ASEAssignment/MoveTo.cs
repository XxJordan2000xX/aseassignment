﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// Class containing the methods for moving the Pen.
    /// </summary>
    public class MoveTo
    {
        /// <summary>
        /// Method responsible for moving the pen without drawing.
        /// will move pen by taking the x and y coordinates from the canvas
        /// class and updating them to the new values.
        /// </summary>
        /// <param name="newX">the X coordinate to move to </param>
        /// <param name="newY">the Y coordinate to move to</param>
        /// <param name="MyCanvas">the graphics context that will be updated</param>
        public void positionPen(int newX, int newY,Canvas MyCanvas)
        {
            MyCanvas.X = newX;
            MyCanvas.Y = newY;
        }
    }
}
