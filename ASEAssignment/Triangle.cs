﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    ///This class is responsible for containing the code required to draw a triangle.
    /// </summary>
    class Triangle : Shape
    {
        int lengthAB, lengthAC, AngleA; // the paramaters that will be used to draw the triangle
        /// <summary>
        /// this method contains a blank constructor.
        /// </summary>
        public Triangle() : base()
        {
            lengthAB = lengthAC = 50;
            AngleA = 45;
        }
        /// <summary>
        /// This method contains the constructor for the triangle class 
        /// </summary>
        /// <param name="MyCanvas">the graphics which will be updated </param>
        /// <param name="SideAB">will hold the distance from point A to point B.</param>
        /// <param name="sideAC">will hold the distance from point A to point C.</param>
        /// <param name="AngleA">will hold the angle at point A.</param>
        public Triangle(Canvas MyCanvas, int lengthAB, int lengthAC, int AngleA) : base(MyCanvas)
        {
            this.lengthAB = lengthAB;
            this.lengthAC = lengthAC;
            this.AngleA = AngleA;
        }
        /// <summary>
        /// THis method will set the array list items 
        /// along with calling the preset peramaters from the base class.
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="list">The peramaters that will be passed which
        /// contains the variables for the X,Y, lengthAB,lenghtAC and AngleA</param>
        public override void set(bool fill, Color colour, params int[] list)
        {
            //list[0] is X, list[1] is Y, list[2] is lengthAB,list[3] is lengthAc,list[4] is AngleA,
            base.set(fill, colour, list[0], list[1]);
            this.lengthAB = list[2];
            this.lengthAC = list[3];
            this.AngleA = list[4];
        }
        /// <summary>
        /// Will determain if it needs to draw a triangle filled or unfilled. Then it will 
        /// calculate the coordinates of the 3 points and use the drawPolygon feature in the
        /// graphics to specify these points. It makes use of trigonometric equations to 
        /// determain where the points should be.
        /// 
        /// Note: angleA must be converted to rads from degrees.
        /// 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="g">The graphics context which the shape will be drawn onto</param>
        public override void draw(bool fill, Color colour, Graphics g)
        {
            Pen pen = new Pen(colour, 1);
            SolidBrush brush = new SolidBrush(Color.Black);
            if (fill)
            {
                g.FillPolygon(brush, new Point[]
                {
                    new Point(X,Y),
                    new Point(X+lengthAB,Y),
                    new Point((int)(X+lengthAC*Math.Cos(AngleA*Math.PI/180)),
                              (int)(Y+lengthAC*Math.Sin(AngleA*Math.PI/180))),
                 });
            }
            else
            {
                g.DrawPolygon(pen, new Point[]
                {
                    new Point(X,Y),
                    new Point(X+lengthAB,Y),
                    new Point((int)(X+lengthAC*Math.Cos(AngleA*Math.PI/180)),
                              (int)(Y+lengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
            }
        }
    } 
}
