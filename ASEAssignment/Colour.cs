﻿using System;
using System.Drawing;

namespace ASEAssignment
{
    /// <summary>
    /// class containing the methods for altering the colours in which the graphic will be dispalyed
    /// </summary>
    class Colour
    {
        //string of all the colours that the pen can be 
        public String[] pencolours = { "red", "blue", "green", "black", "yellow", "purple", "pink", "orange" };

        /// <summary>
        /// This method is responsible for changing the colour of the pen by updating
        /// the pen color stored in the canvas class.
        /// </summary>
        /// <param name="penColour">the colour the pen will be changed to</param>
        /// <param name="MyCanvas">The canvas context to update</param>       
        public void penColour(String penColour, Canvas MyCanvas)
        {
            if (penColour == "red")
            {
                MyCanvas.pen = new Pen(Color.Red, 1);
                MyCanvas.brush = new SolidBrush(Color.Red);
            }
            else if (penColour == "blue")
            {
                MyCanvas.pen = new Pen(Color.Blue, 1);
                MyCanvas.brush = new SolidBrush(Color.Blue);
            }
            else if (penColour == "green")
            {
                MyCanvas.pen = new Pen(Color.Green, 1);
                MyCanvas.brush = new SolidBrush(Color.Green);
            }
            else if (penColour == "black")
            {
                MyCanvas.pen = new Pen(Color.Black, 1);
                MyCanvas.brush = new SolidBrush(Color.Black);
            }
            else if (penColour == "yellow")
            {
                MyCanvas.pen = new Pen(Color.Yellow, 1);
                MyCanvas.brush = new SolidBrush(Color.Black);
            }
            else if (penColour == "purple")
            {
                MyCanvas.pen = new Pen(Color.Purple, 1);
                MyCanvas.brush = new SolidBrush(Color.Black);
            }
            else if (penColour == "pink")
            {
                MyCanvas.pen = new Pen(Color.Pink, 1);
                MyCanvas.brush = new SolidBrush(Color.Black);
            }
            else if (penColour == "orange")
            {
                MyCanvas.pen = new Pen(Color.Pink, 1);
                MyCanvas.brush = new SolidBrush(Color.Black);
            }
        }

        /// <summary>
        /// this class will determain if the user has set fill to on or off 
        /// and update the canvas class so that the shapes classes will know which drawing function to call.
        /// </summary>
        /// <param name="fillColour">will determain if fill is on or off</param>
        /// <param name="MyCanvas"> the graphics context that will be updated</param>
        public void fillColour(String fillColour, Canvas MyCanvas)
        {
            if (fillColour == "on")
            {
                MyCanvas.fill = true;
            }
            else if (fillColour == "off")
            {
                MyCanvas.fill = false;
            }
        }
    }
}
