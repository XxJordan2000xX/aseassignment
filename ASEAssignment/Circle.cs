﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// This class is used to draw a circle on the form window.
    /// </summary>
    class Circle : Shape
    {
        int radius; // stores the radius of the circle 
        /// <summary>
        /// blank circle constructor
        /// </summary>
        public Circle():base()
        {
            radius = 50;
        }
        /// <summary>
        /// This method is the constructor for the circle class.
        /// </summary>
        /// <param name="MyCanvas">used to get the variables which are being updated from the canvas which is being updated</param>
        /// <param name="radius">the peramater that will be used to draw the circle.</param>
        public Circle(Canvas MyCanvas, int radius):base(MyCanvas)
        {
            this.radius = radius;
        }
        /// <summary>
        /// This method will be used to set the shapes peramaters 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="list">variable list as some shapes have more peramaters than others</param>
        public override void set(bool fill, Color colour,params int[] list)
        {
            //list[0] is X, list[1] is Y, list[2] is radius
            base.set(fill,colour,list[0],list[1]);
            this.radius = list[2];
        }
        /// <summary>
        /// Will determain if it needs to draw a circle filled or unfilled. Then it will 
        /// use the draw/fill Ellipse feature in the graphics to draw the shape. 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="g">The graphics context which the shape will be drawn onto</param>
        public override void draw(bool fill,Color colour,Graphics g)
        {
            Pen pen = new Pen(colour, 1);
            SolidBrush brush = new SolidBrush(Color.Black);
            if (fill)
            {
                g.FillEllipse(brush,X,Y,radius,radius); //will draw a filled circle    
            }
            else
            { 
               g.DrawEllipse(pen,X,Y,radius,radius); //will draw a circle 
            }
        }
        
    }
}
