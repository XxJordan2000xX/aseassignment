﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEAssignment
{
    /// <summary>
    /// This class hadles all the events occuring within the form.
    /// </summary>
    public partial class Application : Form
    {
  
        static String userIn; //temporarily holds the users commands
        static String progIn; //gets program code

        string message; //the responce to the users console
        bool execute = true; //true if executing code false if just testing code

        Canvas Mycanvas; 
        Bitmap OutputBitmap; //bitmap to draw on which will display in the form panel 


        /// <summary>
        /// This method initialises the form.
        /// </summary>
        public Application()
        {
            InitializeComponent();
            int ScreenX = Canvas_window.Width;
            int ScreenY = Canvas_window.Height;
            OutputBitmap = new Bitmap(ScreenX, ScreenY);
            Mycanvas = new Canvas(Graphics.FromImage(OutputBitmap)); //passes bitmap graphic context to Mycanvas object
        }

        /// <summary>
        /// If the user clicks the Run button this code will execute.
        /// It makes a call to the Parser class to process the users input.
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(the button)</param>
        /// <param name="e">the event data.(button clicked)</param>
        private void Run_Click(object sender, EventArgs e)
        {
            execute = true;
            Parser command = new Parser();
            progIn = UserProgram.Text;
            message = command.Read(progIn,Mycanvas, execute);
            Refresh();
            UserConsole.AppendText(message);
        }

        /// <summary>
        /// This method is called if the user hits the enter key.
        /// This method will call the Parser class and process the users input.
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(the richtextbox)</param>
        /// <param name="e">the event data.(key pressed)</param>
        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                execute = true;
                Parser command = new Parser();
                userIn = CommandLine.Text.Trim().ToLower();
                if (userIn == "run")
                {
                    progIn = UserProgram.Text;
                    message = command.Read(progIn,Mycanvas, execute);
                    
                }
                else
                {
                    message = command.Read(userIn,Mycanvas, execute);
                }              
                UserConsole.AppendText(message);
                CommandLine.Text = "";
                Refresh();
            }
        }

        /// <summary>
        /// When called allows you to draw on the bitmap and puts it at position 0,0 in the panel.
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(refresh or an update)</param>
        /// <param name="e">the event data.(paint canvas)</param>
        private void Canvas_window_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics; // gets graphics context of the form 
            g.DrawImageUnscaled(OutputBitmap,0,0); //put the bitmap on the form.  
        }

        /// <summary>
        /// this event will close the program when the user clicks exit from the menu
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(menu item exit)</param>
        /// <param name="e">the event data.(exit clicked)</param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// this event will allow the user to select and load their program 
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(load menu item)</param>
        /// <param name="e">the event data.(load clicked)</param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create an OpenFileDialog to request a file to open.
            OpenFileDialog openFile = new OpenFileDialog();

            // Initialize the OpenFileDialog to look for RTF files.
            openFile.DefaultExt = "*.rtf";
            openFile.Filter = "RTF Files|*.rtf";

            // Determine whether the user selected a file from the OpenFileDialog.
            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
               openFile.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                UserProgram.LoadFile(openFile.FileName, RichTextBoxStreamType.PlainText);
            }
        }
        /// <summary>
        /// this event will allow a user to save their new code to a file.
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(save menu item)</param>
        /// <param name="e">the event data.(save -> save as clicked)</param>
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create a SaveFileDialog to request a path and file name to save to.
            SaveFileDialog saveFile = new SaveFileDialog();

            // Initialize the SaveFileDialog to specify the RTF(rich text format) extension for the file.
            saveFile.DefaultExt = "*.rtf";
            saveFile.Filter = "RTF Files|*.rtf";

            // Determine if the user selected a file name from the saveFileDialog.
            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
               saveFile.FileName.Length > 0)
            {
                // Save the contents of the RichTextBox into the file.
                UserProgram.SaveFile(saveFile.FileName, RichTextBoxStreamType.PlainText);
            }
        }
        /// <summary>
        /// when test is clicked the syntex of the program will be checked and errors will 
        /// be reported without running the code.
        /// 
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(test button)</param>
        /// <param name="e">the event data.(test button clicked)</param>
        private void Test_Click(object sender, EventArgs e)
        {
            execute = false;
            Parser command = new Parser();
            progIn = UserProgram.Text;
            message = command.Read(progIn, Mycanvas, execute);
            Refresh();
            UserConsole.AppendText(message);
        }

        /// <summary>
        /// when this menu item is clicked the program will reset the canvas to blank at position 0,0.
        /// It will also clear the program interface and the command line for the user
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(new menu item)</param>
        /// <param name="e">the event data.(new clicked)</param>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

            execute = true;
            Parser command = new Parser();
            string[] inputs = { "clear", "reset" };
            foreach (var input in inputs) 
            {
                message = command.Read(input, Mycanvas, execute); 
            }
            Refresh();
            UserProgram.Clear();
            UserConsole.Clear();
        }
        /// <summary>
        /// If the user clicks the Run button this code will execute.
        /// It makes a call to the Parser class to process the users input.
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(the menu item)</param>
        /// <param name="e">the event data.(menu item clicked)</param>
        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            execute = true;
            Parser command = new Parser();
            progIn = UserProgram.Text;
            message = command.Read(progIn, Mycanvas, execute);
            Refresh();
            UserConsole.AppendText(message);
        }
        /// <summary>
        /// when test is clicked the syntex of the program will be checked and errors will 
        /// be reported without running the code.
        /// </summary>
        /// <param name="sender">reference to the object that raised the event.(test menu item)</param>
        /// <param name="e">the event data.(test menu item clicked)</param>
        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            execute = false;
            Parser command = new Parser();
            progIn = UserProgram.Text;
            message = command.Read(progIn, Mycanvas, execute);
            Refresh();
            UserConsole.AppendText(message);
        }
        /// <summary>
        /// When the help menu item is cliked a help popup will appear showing some of the
        /// basic commands that can be ran and the format in which to enter them.
        /// </summary>
        /// <param name="sender">referes to object that raised event(view help menu item )</param>
        /// <param name="e">the event data.(view help menu item clicked)</param>
        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String help = "Some basic commands that can be ran:\n" +
                "To move the cursor without drawing: moveto <X>,<Y>  \n" +
                "To draw a line: drawto <X>,<Y>\n" +
                "To draw a circle:  circle <Radius> \n" +
                "To draw a rectangle:  rectangle <Width>,<Height> \n" +
                "To draw a triangle: triangle <LenghtAB>,<LenghtAC>,<AngleA> \n" +
                "To clear the drawing area: clear \n" +
                "To reset the pen to 0,0: reset \n " +
                "To change the pen colour: pen <colour>\n " +
                "To toggle fill on or off: fill <on/off>\n " +
                "For further help and information please consult the documentation ";
            MessageBox.Show(help,"HELP WINDOW");
        }
    }

}
