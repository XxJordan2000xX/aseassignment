﻿using System;
using System.Collections;
using System.Data;
using System.Linq;

namespace ASEAssignment
{
    /// <summary>
    /// 
    /// this class will take in the users commands, split them as necessary and process them to 
    /// determain which method to call it will also catch errors when incorrect commands are entered.
    /// This class will take note of the linenumber the command is on this is so that it can report
    /// helpful errors back to the user.
    /// 
    /// </summary>
    public class Parser
    {
        private string[] userCommand; // used to temporarily save the command
        private ArrayList Commands = new ArrayList(); // an array of the valid commands
        private ArrayList Paramaters = new ArrayList(); // an array of the associated paramaters
        private string Params; // will temporarily store the paramaters 
        private int LineNumber = 0; //the line that is being executed
        private ArrayList variables = new ArrayList();//temporary store of variable names
        private ArrayList methodNames = new ArrayList();//temporary store of method names
        private ArrayList methodParams = new ArrayList();//temporary store of method params

        //lits of all the different possible commands
        private string[] listOfCommands = { "arc", "square", "triangle", "rectangle", "circle", "pen", "fill", "reset", "clear", "var", "drawto", "moveto", "call", "while", "method", "endmethod", "endwhile", "if", "endif" };

        private String message; // the message that is returned after checking of each command  
        private String messages; // the message returned to the user

        // informing if an error has occured or not
        public bool Error { get => error; set => error = value; }
        private bool error = false;

        /// <summary>
        /// This method will read through the program check the syntex of the commands and save them to a 
        /// commands array before calling a method in the storedcode class for executing the commands.
        ///
        /// </summary>
        /// <param name="userIn">A string of the content in the commandLine or program</param>
        /// <param name="Mycanvas">The canvas that will be updated when commands are executed</param>
        /// <param name="execute">A boolean indicating if the execute method needs to be called</param>
        /// <returns>A sting containing information about any errors and the commands that will be executed</returns>
        public string Read(String userIn, Canvas MyCanvas, Boolean execute)
        {
            //splits the user commands based on the line
            userCommand = userIn.Trim().ToLower().Split('\n');
            foreach (var Command in userCommand)
            {
                try
                {
                    //gets the line the command is on
                    LineNumber++;
                    Params = ""; //resets the variable
                    message = ""; 
                    if (String.IsNullOrEmpty(Command) == true)
                    {
                        //empty if statement so lineNumber will be accurate and account for blank lines
                        message = null;
                        error = false;
                    }
                    else if (Command.Contains("var"))
                    {
                        //decleration of variable
                        string[] var = Command.Split(' ');
                        if (var[0] == "var")
                        {
                            //catches if the variable is already declaired
                            if (variables.Contains(var[1]))
                            {
                                //if true throw an error
                                error = true;
                                throw new ApplicationException("on line" + $"{LineNumber}" + "variable is already declaired");
                            }
                            if (listOfCommands.Contains(var[1]))
                            {
                                error = true;
                                throw new ApplicationException("cannot give a variable the same name as a command on line" + $"{LineNumber}");
                            }
                            //checks if variable has a value/expression assigned to it 
                            else if (var.Length > 3 && var[2] == "=")
                            {
                                //saves the name of the variable temporarily
                                string commandName = var[0];
                                int name = 0;
                                //prevents the variable name from being an integer                             
                                bool successName = int.TryParse(var[1], out name);
                                if (successName)
                                {
                                    error = true;
                                    throw new ApplicationException("cannot give a variable an integer as a name on line" + $"{LineNumber}");
                                }
                                else
                                {
                                    string varName = var[1];
                                    //indexes to remove to get the expression
                                    int remove = 0;
                                    int indexToRemove = 0;
                                    //removes var,name and =
                                    while (remove < 3)
                                    {
                                        var = var.Where((source, index) => index != indexToRemove).ToArray();
                                        remove++;
                                    }
                                    //checks if value is an integer and if expression is valid
                                    int counter = 0; //should it be a param or operand
                                    ArrayList expressions = new ArrayList();
                                    foreach (var value in var)
                                    {
                                        //checks if variable is assigned a valid expression eg var num1 = num1 + 10
                                        int number;
                                        //is the value an integer
                                        bool success = int.TryParse(value, out number);
                                        //if value not an integer 
                                        if (!success)
                                        {
                                            if (counter % 2 == 0)
                                            {
                                                //prevents variables from being declaired with the same name as other commands
                                                if (listOfCommands.Contains(value))
                                                {
                                                    error = true;
                                                    throw new ApplicationException("cannot give a variable the same name as a command on line" + $"{LineNumber}");
                                                }
                                                //check if its a variable 
                                                if (variables.Contains(value))
                                                {
                                                    error = false;
                                                    expressions.Add(value + " ");
                                                }
                                                //catches invalid paramater
                                                else
                                                {
                                                    error = true;
                                                    throw new ApplicationException("invalid paramter given to variable decleration on line" + $"{LineNumber}");
                                                }
                                                counter = counter + 1;
                                            }
                                            else
                                            {
                                                //check if its a operand eg + or -
                                                if (value == "+" || value == "-" || value == "*" || value == "/" || value == "%")
                                                {
                                                    error = false;
                                                    expressions.Add(value + " ");
                                                }
                                                else
                                                {
                                                    error = true;
                                                    throw new ApplicationException("invalid operand given in variable decleration on line" + $"{LineNumber}");
                                                }
                                                counter = counter + 1;
                                            }
                                        }
                                        else
                                        {
                                            error = false;
                                            expressions.Add(value + " ");
                                            counter = counter + 1;
                                        }
                                    }
                                    if (!error)
                                    {
                                        Commands.Add(commandName);
                                        Params = varName + "=";
                                        foreach (var ex in expressions)
                                        {
                                            Params = Params + ex + " ";
                                        }
                                        Paramaters.Add(Params);
                                        variables.Add(varName);
                                    }
                                }

                            }
                            //variable without any paramaters eg. var num1
                            else if (var.Length == 2)
                            {
                                Commands.Add(var[0]);
                                Paramaters.Add(var[1] + "=" + "0");
                                variables.Add(var[1]);
                            }
                            else
                            {
                                error = true;
                                throw new ApplicationException("variable requires a name on line" + $"{LineNumber}");
                            }
                        }
                        else
                        {
                            error = true;
                            throw new ApplicationException("invalid variable decleration on line" + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("method"))
                    {
                        if (Command == "endmethod")
                        {
                            error = false;
                            //adds the command to the dictionary
                            Commands.Add(Command);
                            Paramaters.Add("0");
                            //message informing about command added to list
                            message = "end of method decleration";
                        }
                        else
                        {
                            int number;
                            String[] method = Command.Split(' ');
                            //method without paramaters
                            if (method[0] == "method" && method.Length == 2)
                            {
                                bool success = int.TryParse(method[1], out number);
                                if (success)
                                {
                                    error = true;
                                    throw new ApplicationException("cannot name a method a number on line" + $"{LineNumber}");
                                }
                                else
                                {
                                    error = false;
                                    methodNames.Add(method[1]);
                                    Params = $"{method[1]}";
                                    //adds the command and method name to the dictionary
                                    Commands.Add(method[0]);
                                    Paramaters.Add(Params);
                                    //message informing about command added to list
                                    message = "begining of method decleration";
                                }
                            }                           
                            else
                            {
                                error = true;
                                throw new ApplicationException("invalid method decleration on line" + $"{LineNumber}");
                            }
                        }
                    }
                    else if (Command.Contains("call"))
                    {
                        string[] methodCall = Command.Split(' ');
                        if (methodCall[0] == "call" && methodNames.Contains(methodCall[1]))
                        {
                            error = false;
                            Params = $"{methodCall[1]}";
                            //adds the command and method name to the dictionary
                            Commands.Add(methodCall[0]);
                            Paramaters.Add(Params);
                            //message informing about command added to list
                            message = "calling a method";
                        }
                        else
                        {
                            error = true;
                            throw new ApplicationException("invalid method call on line " + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("if"))
                    {
                        if (Command == "endif")
                        {
                            error = false;
                            //adds the command to the dictionary
                            Commands.Add(Command);
                            Paramaters.Add(null);
                            //message informing about command added to list
                            message = "end of if statement";
                        }
                        else
                        {
                            String[] ifStatement = Command.Split(' ');
                            ArrayList ifcondition = new ArrayList();
                            string conditions = "";
                            if (ifStatement[0] == "if" && ifStatement.Length > 2)
                            {
                                string c = ifStatement[0]; //command being called
                                //removes if
                                int remove = 0;
                                ifStatement = ifStatement.Where((source, index) => index != remove).ToArray();
                                //checks if it is a valid expression
                                int counter = 0;//will check if it should be a param or an operator
                                foreach (var value in ifStatement)
                                {
                                    int number;

                                    //is the value an integer
                                    bool success = int.TryParse(value, out number);
                                    if (!success)
                                    {
                                        if (counter % 2 == 0)
                                        {
                                            //check if its a variable 
                                            if (variables.Contains(value))
                                            {
                                                ifcondition.Add(value + " ");
                                                error = false;
                                            }
                                            //catches invalid paramater
                                            else
                                            {
                                                error = true;
                                                throw new ApplicationException("invalid paramter given in expression on line" + $"{LineNumber}");
                                            }
                                            counter = counter + 1;
                                        }
                                        else
                                        {
                                            //check if its a comparitive operator
                                            if (value == "<" || value == ">" || value == "<=" || value == ">=" || value == "==" || value == "!=")
                                            {
                                                ifcondition.Add(value + " ");
                                                error = false;
                                            }
                                            else
                                            {
                                                error = true;
                                                throw new ApplicationException("invalid operand given in expression on line" + $"{LineNumber}");
                                            }
                                            counter = counter + 1;
                                        }
                                    }
                                    else
                                    {
                                        if (counter % 2 == 0)
                                        {
                                            error = false;
                                            ifcondition.Add(value + " ");
                                        }
                                        else
                                        {
                                            error = true;
                                            throw new ApplicationException("parameter in wrong place in expression on line" + $"{LineNumber}");
                                        }
                                        counter = counter + 1;
                                    }
                                }
                                if (!error)
                                {
                                    foreach (var condition in ifcondition)
                                    {
                                        conditions = conditions + condition;
                                    }
                                    //adds the command and condition  to the dictionary
                                    Commands.Add(c);
                                    Paramaters.Add(conditions);
                                    //message informing about command added to list
                                    message = "begining of if statement";
                                }
                            }
                            else
                            {
                                error = true;
                                throw new ApplicationException("invalid if statement on line" + $"{LineNumber}");
                            }
                        }
                    }
                    else if (Command.Contains("while"))
                    {
                        if (Command == "endwhile")
                        {
                            error = false;
                            //adds the command to the dictionary
                            Commands.Add(Command);
                            Paramaters.Add(null);
                            //message informing about command added to list
                            message = "end of while loop";
                        }
                        else
                        {
                            String[] loop = Command.Split(' ');
                            ArrayList condition = new ArrayList();
                            //method without paramaters
                            if (loop[0] == "while" && loop.Length > 2)
                            {
                                //command being called
                                string L = loop[0];
                                //removes while
                                int remove = 0;
                                loop = loop.Where((source, index) => index != remove).ToArray();
                                //checks if it is a valid expression
                                int counter = 0; // is it an odd or even param 
                                foreach (var value in loop)
                                {
                                    int number;

                                    //is the value an integer
                                    bool success = int.TryParse(value, out number);
                                    if (!success)
                                    {
                                        if (counter % 2 == 0)
                                        {
                                            counter = counter + 1;
                                            if (variables.Contains(value))
                                            {
                                                error = false;
                                                condition.Add(value);
                                            }
                                            else
                                            {
                                                error = true;
                                                throw new ApplicationException("invalid paramater given in expression on line" + $"{LineNumber}");
                                            }
                                        }
                                        else
                                        {
                                            counter = counter + 1;
                                            //check if its a comparitive operator
                                            if (value == "<" || value == ">" || value == "<=" || value == ">=" || value == "==" || value == "!=")
                                            {
                                                condition.Add(value);
                                                error = false;
                                            }//catches invalid paramater
                                            else
                                            {
                                                error = true;
                                                throw new ApplicationException("invalid operand in expression on line" + $"{LineNumber}");
                                            }
                                        }


                                    }
                                    else
                                    {
                                        if (counter % 2 == 0)
                                        {
                                            error = false;
                                            condition.Add(value);
                                        }
                                        else
                                        {
                                            error = true;
                                            throw new ApplicationException("invalid position for paramater in expression on line" + $"{LineNumber}");
                                        }
                                        counter = counter + 1;
                                    }
                                }
                                if (!error)
                                {
                                    foreach (var l in condition)
                                    {
                                        Params = Params + l + " ";
                                    }
                                    //adds the command and condition  to the dictionary
                                    Commands.Add(L);
                                    Paramaters.Add(Params);
                                    //message informing about command added to list
                                    message = "begining of while loop";
                                }
                            }
                            else
                            {
                                error = true;
                                throw new ApplicationException("invalid while loop on line" + $"{LineNumber}");
                            }
                        }
                    }
                    else if (Command.Contains("moveto"))
                    {
                        //splits the command
                        String[] movepen = Command.Split(' ', ',');
                        //checks the command is of an appropriate number of variables 
                        if (movepen[0] == "moveto" && movepen.Length == 3)
                        {
                            int newX, newY;
                            ArrayList XY = new ArrayList();
                            //is the value an integer
                            bool successX = int.TryParse(movepen[1], out newX);
                            if (!successX)
                            {
                                //is it a variable
                                if (variables.Contains(movepen[1]))
                                {
                                    error = false;
                                    XY.Add(movepen[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the X coordinate on line" + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (newX >= 0)
                                {
                                    error = false;
                                    XY.Add(movepen[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive value for X on line" + $"{LineNumber}");
                                }
                            }
                            bool successY = int.TryParse(movepen[2], out newY);
                            if (!successY)
                            {
                                //is it a variable
                                if (variables.Contains(movepen[2]))
                                {
                                    error = false;
                                    XY.Add(movepen[2]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the Y coordinate on line" + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (newY >= 0)
                                {
                                    error = false;
                                    XY.Add(movepen[2]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive value for Y on line" + $"{LineNumber}");
                                }
                            }
                            if (!error)
                            {
                                foreach (var coordinate in XY)
                                {
                                    Params = Params + coordinate;
                                }
                                //adds the command and paramaters to the dictionary
                                Commands.Add(movepen[0]);
                                Paramaters.Add(Params);
                                //adds the valid command to list of messages to display to console
                                message = "moveto " + $"{movepen[1]}" + "," + $"{movepen[2]}";
                            }
                        }
                        else
                        {
                            error = true;
                            throw new ApplicationException("Invalid command on line" + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("drawto"))
                    {
                        //splits the command
                        String[] pendraw = Command.Split(' ', ',');
                        //checks the command is of an appropriate number of variables 
                        if (pendraw[0] == "drawto" && pendraw.Length == 3)
                        {
                            ArrayList XY = new ArrayList();
                            int newX, newY;
                            //is the value an integer
                            bool successX = int.TryParse(pendraw[1], out newX);
                            if (!successX)
                            {

                                //is it a variable
                                if (variables.Contains(pendraw[1]))
                                {
                                    error = false;
                                    XY.Add(pendraw[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the X coordinate on line" + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (newX >= 0)
                                {
                                    error = false;
                                    XY.Add(pendraw[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive value for X on line" + $"{LineNumber}");
                                }
                            }
                            bool successY = int.TryParse(pendraw[2], out newY);
                            if (!successY)
                            {
                                //is it a variable
                                if (variables.Contains(pendraw[2]))
                                {
                                    error = false;
                                    XY.Add(pendraw[2]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the Y coordinate on line" + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (newY >= 0)
                                {
                                    error = false;
                                    XY.Add(pendraw[2]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive value for Y on line" + $"{LineNumber}");
                                }
                            }
                            if (!error)
                            {
                                foreach (var coordinate in XY)
                                {
                                    Params = Params + coordinate;
                                }
                                Commands.Add(pendraw[0]);
                                Paramaters.Add(Params);
                                //adds the valid command to list of messages to display to console
                                message = "drawto " + $"{pendraw[1]}" + "," + $"{pendraw[2]}";
                            }
                        }
                        else
                        {
                            error = true;
                            throw new ApplicationException("Invalid command on line" + $"{LineNumber}");
                        }
                    }
                    else if (Command == "clear")
                    {
                        error = false;
                        //adds the command to the dictionary
                        Commands.Add(Command);
                        Paramaters.Add(null);
                        //message informing about command added to list
                        message = "clear";
                    }
                    else if (Command == "reset")
                    {
                        error = false;
                        //adds the command to the dictionary
                        Commands.Add(Command);
                        Paramaters.Add(null);
                        //message informing about command added to list
                        message = "reset";
                    }
                    else if (Command.Contains("fill"))
                    {
                        //splits the command
                        String[] fillState = Command.Trim().Split(' ');
                        //does the command have the correct number of elements
                        if (fillState[0] == "fill" && fillState.Length == 2)
                        {
                            //is the state a valid one
                            if (fillState[1] == "on" || fillState[1] == "off")
                            {
                                error = false;
                                //creates a string of the paramaters
                                Params = $"{fillState[1]}";
                                //adds the command and paramaters to the dictionary
                                Commands.Add(fillState[0]);
                                Paramaters.Add(Params);
                                //message informing about command added to list
                                message = "fill " + $"{fillState[1]}";
                            }
                            else
                            {
                                error = true;
                                //returns and error if they havent specified on or off
                                throw new ApplicationException("please specify if you want fill on or off on line " + $"{LineNumber}");
                            }
                        }
                        else
                        {
                            error = true;
                            //returns an error for invalid command with fill in it 
                            throw new ApplicationException("Invalid command on line on line" + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("pen"))
                    {
                        //splits the command
                        String[] penColour = Command.Trim().Split(' ');
                        //checks the number of paramters
                        if (penColour[0] == "pen" && penColour.Length == 2)
                        {
                            //checks that they can change to the pen colour they would like to 
                            Colour penc = new Colour();
                            if (penc.pencolours.Contains(penColour[1]))
                            {
                                error = false;
                                //creates a string of the paramaters
                                Params = $"{penColour[1]}";
                                //adds the command and paramaters to the dictionary
                                Commands.Add(penColour[0]);
                                Paramaters.Add(Params);
                                message = "pen colour set to " + $"{penColour[1]}";
                            }
                            else
                            {
                                error = true;
                                //message if colour isn't possible to change to.
                                throw new ApplicationException("please enter a valid colour on line " + $"{LineNumber}");
                            }
                        }
                        else
                        {
                            error = true;
                            //message returned if they have specified a colour incorrectly 
                            throw new ApplicationException("Invalid command on line" + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("circle"))
                    {
                        //splits the command
                        String[] circle = Command.Trim().Split(' ');
                        //checks command has the correct number of paramaters
                        if (circle[0] == "circle" && circle.Length == 2)
                        {
                            //is a variable being used as the paramter
                            if (variables.Contains(circle[1]))
                            {
                                error = false;
                                //creates a string of the paramaters
                                Params = $"{circle[1]}";
                                //adds the command and paramaters to the array lists
                                Commands.Add(circle[0]);
                                Paramaters.Add(Params);
                                //message informing about command added to list
                                message = "circle " + $"{circle[1]}";
                            }
                            else
                            {
                                //checks the value for the radius is a integer
                                int radius = Int32.Parse(circle[1]);
                                //checks the int is greter than 0
                                if (radius > 0)
                                {
                                    error = false;
                                    //creates a string of the paramaters
                                    Params = $"{circle[1]}";
                                    //adds the command and paramaters to the array lists
                                    Commands.Add(circle[0]);
                                    Paramaters.Add(Params);
                                    //message informing about command added to list
                                    message = "circle " + $"{radius}";
                                }
                                else
                                {
                                    error = true;
                                    //error added to list displayed to console
                                    throw new ApplicationException("please enter a positive radius on line " + $"{LineNumber}");
                                }
                            }
                        }
                        else
                        {
                            error = true;
                            //error added to list displayed to console
                            throw new ApplicationException("Invalid command on line " + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("rectangle"))
                    {
                        //splits the command
                        String[] rectangle = Command.Split(' ', ',');
                        //checks correct number of paramaters
                        if (rectangle[0] == "rectangle" && rectangle.Length == 3)
                        {
                            ArrayList WidthHeight = new ArrayList();
                            int width, height;
                            //is the value an integer
                            bool successW = int.TryParse(rectangle[1], out width);
                            if (!successW)
                            {
                                //is it a variable
                                if (variables.Contains(rectangle[1]))
                                {
                                    error = false;
                                    //Params = Params + $"{rectangle[1]}";
                                    WidthHeight.Add(rectangle[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the width on line " + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (width > 0)
                                {
                                    error = false;
                                    WidthHeight.Add(width + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive width on line " + $"{LineNumber}");
                                }
                            }
                            bool successH = int.TryParse(rectangle[2], out height);
                            if (!successH)
                            {
                                //is it a variable
                                if (variables.Contains(rectangle[2]))
                                {
                                    error = false;
                                    WidthHeight.Add(rectangle[2]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the width on line " + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (height > 0)
                                {
                                    error = false;
                                    WidthHeight.Add(height);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive height on line " + $"{LineNumber}");
                                }
                            }
                            if (!error)
                            {
                                foreach (var wh in WidthHeight)
                                {
                                    Params = Params + wh;
                                }
                                Commands.Add(rectangle[0]);
                                Paramaters.Add(Params);
                                //message informing about command added to list
                                message = "rectangle " + $"{rectangle[1]}" + "," + $"{rectangle[2]}";
                            }
                            else
                            {
                                error = true;
                                //error added to list displayed to console
                                throw new ApplicationException("please enter a valid length and height on line " + $"{LineNumber}");
                            }
                        }
                        else
                        {
                            error = true;
                            //error added to list displayed to console
                            throw new ApplicationException("Invalid command on line on line " + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("triangle"))
                    {
                        ArrayList LengthsAngle = new ArrayList();
                        //splits the command
                        String[] triangle = Command.Trim().Split(' ', ',');
                        //checks correct number of params
                        if (triangle[0] == "triangle" && triangle.Length == 4)
                        {
                            int lengthAB, lengthAC, angleA;
                            Params = null;
                            //is the value an integer
                            bool successAB = int.TryParse(triangle[1], out lengthAB);
                            if (!successAB)
                            {
                                //is it a variable
                                if (variables.Contains(triangle[1]))
                                {
                                    error = false;
                                    LengthsAngle.Add(triangle[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for lengthAB on line " + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (lengthAB > 0)
                                {

                                    error = false;
                                    LengthsAngle.Add(triangle[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive length for AB on line " + $"{LineNumber}");
                                }
                            }
                            bool successAC = int.TryParse(triangle[2], out lengthAC);
                            if (!successAC)
                            {
                                //is it a variable
                                if (variables.Contains(triangle[2]))
                                {
                                    error = false;
                                    LengthsAngle.Add(triangle[2] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for lengthAC on line " + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (lengthAC > 0)
                                {

                                    error = false;
                                    LengthsAngle.Add(triangle[2] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive length for AC on line " + $"{LineNumber}");
                                }
                            }
                            bool successA = int.TryParse(triangle[3], out angleA);
                            if (!successA)
                            {
                                //is it a variable
                                if (variables.Contains(triangle[3]))
                                {
                                    error = false;
                                    LengthsAngle.Add(triangle[3]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for angleA on line " + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (angleA > 0 && angleA < 180)
                                {

                                    error = false;
                                    LengthsAngle.Add(triangle[3]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive angle for angleA between 0 and 180 on line " + $"{LineNumber}");
                                }
                            }
                            if (!error)
                            {
                                foreach (var la in LengthsAngle)
                                {
                                    Params = Params + la;
                                }
                                Commands.Add(triangle[0]);
                                Paramaters.Add(Params);
                                //message informing about command added to list
                                message = "triangle " + $"{triangle[1]}," + $"{triangle[2]}," + $"{triangle[3]}";
                            }
                        }
                        else
                        {
                            error = true;
                            throw new ApplicationException("Invalid command on line" + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("square"))
                    {
                        //splits the command
                        String[] square = Command.Trim().Split(' ');
                        //checks command has the correct number of paramaters
                        if (square[0] == "square" && square.Length == 2)
                        {
                            //is a variable being used as the paramter
                            if (variables.Contains(square[1]))
                            {
                                error = false;
                                //creates a string of the paramaters
                                Params = $"{square[1]}";
                                //adds the command and paramaters to the array lists
                                Commands.Add(square[0]);
                                Paramaters.Add(Params);
                                //message informing about command added to list
                                message = "square " + $"{square[1]}";
                            }
                            else
                            {
                                //checks the value for the width/height is a integer
                                int widthHeight = Int32.Parse(square[1]);
                                //checks the int is greter than 0
                                if (widthHeight > 0)
                                {
                                    error = false;
                                    //creates a string of the paramaters
                                    Params = $"{square[1]}";
                                    //adds the command and paramaters to the array lists
                                    Commands.Add(square[0]);
                                    Paramaters.Add(Params);
                                    //message informing about command added to list
                                    message = "square " + $"{square[1]}";
                                }
                                else
                                {
                                    error = true;
                                    //error added to list displayed to console
                                    throw new ApplicationException("please enter a positive widthHeight on line " + $"{LineNumber}");
                                }
                            }
                        }
                        else
                        {
                            error = true;
                            //error added to list displayed to console
                            throw new ApplicationException("Invalid command on line " + $"{LineNumber}");
                        }
                    }
                    else if (Command.Contains("arc"))
                    {
                        //splits the command
                        String[] pendraw = Command.Split(' ', ',');
                        //checks the command is of an appropriate number of variables 
                        if (pendraw[0] == "arc" && pendraw.Length == 5)
                        {
                            ArrayList A = new ArrayList();
                            int width, height, startAngle, SweepAngle;
                            //is the value an integer
                            bool successW = int.TryParse(pendraw[1], out width);
                            if (!successW)
                            {

                                //is it a variable
                                if (variables.Contains(pendraw[1]))
                                {
                                    error = false;
                                    A.Add(pendraw[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the width on line" + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (width > 0)
                                {
                                    error = false;
                                    A.Add(pendraw[1] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive value for width on line" + $"{LineNumber}");
                                }
                            }
                            bool successH = int.TryParse(pendraw[2], out height);
                            if (!successH)
                            {
                                //is it a variable
                                if (variables.Contains(pendraw[2]))
                                {
                                    error = false;
                                    A.Add(pendraw[2] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for the height on line" + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (height > 0)
                                {
                                    error = false;
                                    A.Add(pendraw[2] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive value for height on line" + $"{LineNumber}");
                                }
                            }
                            bool successStartA = int.TryParse(pendraw[3], out startAngle);
                            if (!successStartA)
                            {
                                //is it a variable
                                if (variables.Contains(pendraw[3]))
                                {
                                    error = false;
                                    A.Add(pendraw[3] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for startAngle on line " + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (startAngle >= 0 && startAngle <= 360)
                                {

                                    error = false;
                                    A.Add(pendraw[3] + ",");
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive angle for startAngle between 0 and 360 on line " + $"{LineNumber}");
                                }
                            }
                            bool successSweepA = int.TryParse(pendraw[4], out SweepAngle);
                            if (!successSweepA)
                            {
                                //is it a variable
                                if (variables.Contains(pendraw[4]))
                                {
                                    error = false;
                                    A.Add(pendraw[4]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("invalid paramater for SweepAngle on line " + $"{LineNumber}");
                                }
                            }
                            else
                            {
                                if (SweepAngle >= 0 && SweepAngle <= 360)
                                {

                                    error = false;
                                    A.Add(pendraw[4]);
                                }
                                else
                                {
                                    error = true;
                                    throw new ApplicationException("please enter a positive angle for SweepAngle between 0 and 360 on line " + $"{LineNumber}");
                                }
                            }
                            if (!error)
                            {
                                foreach (var coordinate in A)
                                {
                                    Params = Params + coordinate;
                                }
                                Commands.Add(pendraw[0]);
                                Paramaters.Add(Params);
                                //adds the valid command to list of messages to display to console
                                message = "Arc " + $"{pendraw[1]}" + "," + $"{pendraw[2]}" + "," + $"{pendraw[3]}" + "," + $"{pendraw[4]}";
                            }
                        }
                        else
                        {
                            error = true;
                            throw new ApplicationException("Invalid command on line" + $"{LineNumber}");
                        }
                    }
                    else
                    {
                        //check for update to already declaired variable
                        String[] var = Command.Split(' ');
                        ArrayList expressions = new ArrayList();
                        if (variables.Contains(var[0]) && var[1] == "=")
                        {
                            //name of variable being updated
                            string name = var[0];
                            //checks paramaters are valid
                            //indexes to remove to get the expression
                            int remove = 0;
                            int indexToRemove = 0;
                            //removes name and =
                            while (remove < 2)
                            {
                                var = var.Where((source, index) => index != indexToRemove).ToArray();
                                remove++;
                            }
                            int counter = 0;
                            foreach (var value in var)
                            {
                                //checks if variable is assigned a valid expression eg var num1 = num1 + 10
                                int number;
                                //is the value an integer
                                bool success = int.TryParse(value, out number);
                                //if value not an integer 
                                if (!success)
                                {
                                    if (counter % 2 == 0)
                                    {
                                        //check if its a variable 
                                        if (variables.Contains(value))
                                        {
                                            expressions.Add(value + " ");
                                            error = false;
                                        }
                                        //catches invalid paramater
                                        else
                                        {
                                            error = true;
                                            throw new ApplicationException("invalid paramter given on line" + $"{LineNumber}");
                                        }
                                        counter = counter + 1;
                                    }
                                    else
                                    {
                                        //check if its a operand eg + or -
                                        if (value == "+" || value == "-" || value == "*" || value == "/" || value == "%")
                                        {
                                            expressions.Add(value + " ");
                                            error = false;
                                        }
                                        else
                                        {
                                            error = true;
                                            throw new ApplicationException("invalid operand given on line" + $"{LineNumber}");
                                        }
                                        counter = counter + 1;
                                    }
                                }
                                else
                                {
                                    error = false;
                                    expressions.Add(value + " ");
                                    counter = counter + 1;
                                }
                            }
                            if (!error)
                            {
                                foreach (var ex in expressions)
                                {
                                    Params = Params + ex + " ";
                                }
                                //adds the new expression the the paramter associated with the command(variable name)
                                Paramaters.Add(Params);
                                Commands.Add(name);
                            }
                        }
                        else
                        {
                            error = true;
                            throw new ApplicationException("Invalid Command on line" + $"{LineNumber}");
                        }
                    }
                    if (message == null || message == "")
                    {
                        messages = messages + message;
                    }
                    else
                    {
                        messages = messages + message + "\n";
                    }
                }
                //will catch an exception if a value is not the correct format
                //eg. the user has entered a char and not an int 
                catch (FormatException)
                {
                    error = true;
                    messages = messages + "Invalid parameters on line " + $"{LineNumber}\n";
                }
                //will catch an exception if the tries to input an invalid
                //command that is out the range of the acceptance 
                catch (System.IndexOutOfRangeException)
                {
                    error = true;
                    messages = messages + "Invalid command on line " + $"{LineNumber}\n";
                }
                //if an error has been detected within the program and an exception has been
                //thrown it will be caught here 
                catch (ApplicationException e)
                {
                    messages = messages + e.Message + "\n";
                }
            }
            //if execute == true then call the code responsible for handling the execution
            if (execute)
            {
                //calls the method for executing the code 
                StoredCode executecode = new StoredCode();
                executecode.process(Commands, Paramaters, MyCanvas);
            }
            //returns the errors and valid commands to the console
            return messages;
        }
    }
}
