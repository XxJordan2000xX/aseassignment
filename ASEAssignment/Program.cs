﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEAssignment
{
    /// <summary>
    /// The Program class holds the code which initialises the program.
    /// By Jordan Drummond Edwards
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main method houses the entry point for the application,
        /// this initialises the program and allows it to execute.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            System.Windows.Forms.Application.Run(new Application());
        }
    }
}
