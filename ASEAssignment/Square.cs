﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// This class is responsible for drawing a squares on the form panel.
    /// </summary>
    class Square : Shape
    {
        int widthHeight; // stores the width/length of the square

        /// <summary>
        /// blank Square constructor
        /// </summary>
        public Square() : base()
        {
            widthHeight = 100;
        }
        /// <summary>
        /// This method contains the constructor for the square class 
        /// </summary>
        /// <param name="MyCanvas">used to get the variables which are being updated from the canvas which is being updated</param>
        /// <param name="widthHeight">will hold the width/Height of the shape </param>
        public Square(Canvas MyCanvas, int widthHeight) : base(MyCanvas)
        {
            this.widthHeight = widthHeight;
        }
        /// <summary>
        /// This method will be used to set the shapes peramaters 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="list">variable list as some shapes have more peramaters than others</param>
        public override void set(bool fill, Color colour, params int[] list)
        {
            //list[0] is X, list[1] is Y, list[2] is widthHeight
            base.set(fill, colour, list[0], list[1]);
            this.widthHeight = list[2];
        }
        /// <summary>
        /// Will determain if it needs to draw a square filled or unfilled. Then it will 
        /// use the draw/fill Rectangle feature in the graphics to draw the shape. 
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="g">The graphics context which the shape will be drawn onto</param>
        public override void draw(bool fill, Color colour, Graphics g)
        {
            Pen pen = new Pen(colour, 1);
            SolidBrush brush = new SolidBrush(Color.Black);
            if (fill)
            {
                g.FillRectangle(brush, X, Y, widthHeight, widthHeight); // will draw a filled square
            }
            else
            {
                g.DrawRectangle(pen, X, Y, widthHeight, widthHeight);//will draw a square
            }

        }
    }
}

