﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignment
{
    /// <summary>
    /// This class holds the code for resetting the position of the pen. 
    /// </summary>
    public class Reset
    {
        /// <summary>
        /// Resets the X and Y position to 0,0 in the top left of the canvas.
        /// </summary>
        /// <param name="myCanvas">passes in the canvas that will be effected.</param>
        public void ResetGraphic(Canvas myCanvas)
        {
            myCanvas.X = myCanvas.Y = 0;           
        }
    }
}
