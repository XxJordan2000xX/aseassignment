﻿using System.Collections.Generic;

namespace ASEAssignment
{
    /// <summary>
    /// This class contains the code associated getting the values of variables 
    /// </summary>
    class Variable
    {
        private int number;
        /// <summary>
        ///  will take a value or variable and return the value to the program for executing
        ///  its command with that paramater.
        /// </summary>
        /// <param name="value">the variable or number being processed</param>
        /// <param name="Variables">a dictionary of the varaibles and associated values</param>
        /// <returns>the value of the variable or the number</returns>
        public int CurrentValue(string value, Dictionary<string, int> Variables)
        {

            //checks if value is an integer
            bool success = int.TryParse(value, out number);
            //if it is just a number
            if (success)
            {
                return number;
            }
            //if it is a variable
            else
            {
                number = Variables[value];
                return number;
            }
        }
    }
}

