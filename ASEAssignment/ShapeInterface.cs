﻿using System.Drawing;

namespace ASEAssignment
{
    /// <summary>
    /// The ShapesInterface contains the methods that force all the objects within
    /// the hierarchy to conform to this template.
    /// </summary>
    interface ShapeInterface
    {
        /// <summary>
        /// This set method will be used to set the paramters required for the shape.
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="list">The peramaters that will be passed which 
        /// will vary as some shapes will have more perameters than others</param>
        void set(bool fill, Color colour, params int[] list);
        /// <summary>
        /// This method is responsible for drawing the shapes.
        /// </summary>
        /// <param name="fill">boolean variable for if shape will be filled or not</param>
        /// <param name="colour">the colour the shape will be</param>
        /// <param name="g">graphics context of where the shape will be drawn</param>
        void draw(bool fill, Color colour, Graphics g);
    }
}
